import java.util.*;

public class q65 {
    public static boolean checkUnique(String n) {
		boolean flag=true;
		for(int i=0;i<n.length();i++) {
			for(int j=i+1;j<n.length();j++) {
				if(n.charAt(i)==n.charAt(j)) {
					flag=false;
				}
			}
		}
		return flag;
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number you want to check: ");
		String n=sc.next();
		int x=Integer.parseInt(n);
		if(x>0 && checkUnique(n)) {
			System.out.println("The number is unique");
		} else {
			System.out.println("The number is not unique");
		}
	}
}
