import java.util.Scanner;

public class q53 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in); 
		System.out.println("Enter nth term: ");
		int n=sc.nextInt();
		for(int i=1;i<=n;i++) {
			int a=(int)Math.pow(i,2);
			int b=(int)Math.pow(i,3);
			if(i==n) System.out.print(a + "," + b);
			else System.out.print(a + "," + b + ",");
		}
	}  
}
