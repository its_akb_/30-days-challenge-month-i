import java.util.Scanner;

public class q4 {

    public static double rectangle(double x1, double x2,double y1,double y2) {
        double res=Math.pow(Math.pow(y1-x1,2)+Math.pow(y2-x2, 2), 0.5);
        return res;
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the coordinates of the four vertices. ");
        System.out.println("Enter coordinate of 1st vertex: ");
        double a1=sc.nextDouble();
        double a2=sc.nextDouble();
        System.out.println("Enter coordinate of 2nd vertex: ");
        double b1=sc.nextDouble();
        double b2=sc.nextDouble();
        System.out.println("Enter coordinate of 3rd vertex: ");
        double c1=sc.nextDouble();
        double c2=sc.nextDouble();
        System.out.println("Enter coordinate of 4th vertex: ");
        double d1=sc.nextDouble();
        double d2=sc.nextDouble();
        double ab=rectangle(a1,a2,b1,b2);
        double bc=rectangle(b1,b2,c1,c2);
        double cd=rectangle(c1,c2,d1,d2);
        double da=rectangle(d1,d2,a1,a2);
        if(ab==bc || ab==cd || ab==da){
            System.out.println("YES ! the points ABCD is a rectangle");
        }
        else{
            System.out.println("NO ! the points ABCD is not a rectangle");
        }
    }
}