import java.util.Scanner;

public class q44 {

    public static boolean ispalindrome(int x,int y) {
		if(x==y) return true;
		else return false;
	}

    public static int dectobin(int n) {
		if(n<=1) return 1;
		else return dectobin(n/2)*10+n%2;
	}

    static int rev=0;
	public static int reverse(int n) {
		if(n>0) {
			int rem=n%10;
			rev=rev*10+rem;
			reverse(n/10);
		}
		else 
			return rev;
		return rev;
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter x:");
		int x=sc.nextInt();
        int ans=dectobin(x);
        int ans2=reverse(ans);
        // System.out.println(x + " " + ans + " " + ans2);
        if(ispalindrome(ans, ans2)) {
            System.out.println("Yes! the binary of " + x + " : " +ans + " is palindrome.");
        } else {
            System.out.println("No! the binary of " + x + " : " +ans + " is not palindrome.");
        }
	}
}
