import java.util.Scanner;

public class q35 {
    
    public static int fibo(int a,int b,int n) {
		if(n<=1) return a;
		else return fibo(b,a+b,n-1);
	}

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter nth term : ");
        int n=sc.nextInt();
        System.out.println("nth term of fibonacci series is : " + fibo(0,1,n));
    }
}