import java.util.*;

public class q75 {
    
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter n : ");
        int n=sc.nextInt();
        int fact=1;
        for(int i=n;i>0;i=i-2) {
            fact=fact*i;
        }
        System.out.println("Double factorial : " + fact);
    }

}
