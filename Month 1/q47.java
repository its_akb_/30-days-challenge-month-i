import java.util.Scanner;

public class q47 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your no : ");
		int n=sc.nextInt();
		int temp=n;
		int sum=0,c;
		while(temp!=0) {
			c=temp%10;
			sum=(int) (sum+Math.pow(c,3));
			temp=temp/10;
		}
		if(sum==n) System.out.println(n + " is a Armstrong Number.");
		else System.out.println(n + " is not a Armstrong Number.");
    }    
}
