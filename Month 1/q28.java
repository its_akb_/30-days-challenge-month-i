import java.util.Scanner;

public class q28 {      
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter n : ");
		int n=sc.nextInt();
		double sum=0,temp=0;
		for(int i=0;i<=n;i++) {
			if(n-i==0) { 
				break;
			}
			else {
				temp=Math.pow((i+1),(n-i));
				sum=sum+temp;
			}
			System.out.println((i+1) + "^" + (n-i) + "	= \t" + temp + "	sum =	" + sum);
		}
	}
}
