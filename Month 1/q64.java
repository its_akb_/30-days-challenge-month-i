import java.util.Scanner;

public class q64 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your no : ");
		String s=sc.next();
		int n=Integer.parseInt(s);
		if(s.length()==4 && n>0) {
				int temp=n;
				int sum=0,c;
				while(temp!=0) {
					c=temp%10;
					sum=sum+c;
					temp=temp/10;
				}
				if(sum%3==0 || sum%5==0 || sum%7==0) {
					System.out.println("Lucky Number.");
				} else {
					System.out.println("Sorry its not my lucky number.");
				}
		} else {
			System.out.println(s + " is not a valid car number.");
		}
    }    
}
