import java.util.*;

public class q30 {
    public static void sort(int[] arr) {
		ArrayList<Integer> res1=new ArrayList<Integer>();
		ArrayList<Integer> res2=new ArrayList<Integer>();
		for(int i=0;i<arr.length;i++) {
			if(arr[i]>0) {
				res1.add(arr[i]);
			} else {
				res2.add(arr[i]);
			}
		}
		res1.addAll(res2);
		// System.out.println(res1);
        for(int i=0;i<res1.size();i++) {
			if(i==res1.size()-1) System.out.print(res1.get(i));
			else System.out.print(res1.get(i) + ", ");
		}
	}	

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size :");
		int n=sc.nextInt();
		int[] arr=new int[n];
		System.out.println("Enter array elements : ");
		for(int i=0;i<n;i++) {
			arr[i]=sc.nextInt();
		}
		sort(arr);
	}
}
