import java.util.Scanner;

public class q82 {

    public static int position(int[] arr) {
        int pos=-1;
        for(int i=0;i<arr.length;i++) {
            for(int j=i+1;j<arr.length;j++) {
                if(arr[i]!=arr[j]) {
                    pos=i;
                }
            }
        }
        return pos;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter n : ");
        int n=sc.nextInt();
        int[] arr=new int[n];
        System.out.println("Enter element : ");
        for(int i=0;i<n;i++) {
            arr[i]=sc.nextInt();
        }
        if(position(arr)!=-1) {
            System.out.println("Transition point : " + (position(arr)+1));
        } else {
            System.out.println("No Transition point " + position(arr));
        }
    }
}