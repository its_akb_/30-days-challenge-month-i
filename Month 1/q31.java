import java.util.Scanner;

public class q31 {

    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int[][] x=new int[4][4];
		int sum=0;
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				System.out.print("Enter (" + i + "," + j + ") element : " );
				x[i][j]=sc.nextInt();
			}
		}
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				System.out.print(x[i][j] + "\t" );
			}
			System.out.println();
		}
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {		
				if(i==0) sum=sum+x[i][j];
				else if(j==0) sum=sum+x[i][j];
				else if(i==3) sum=sum+x[i][j];
				else if(j==3) sum=sum+x[i][j];
			}
		}
		System.out.println("sum = " + sum);
	}
}