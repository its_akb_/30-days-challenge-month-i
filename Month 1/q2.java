import java.util.Scanner;

public class q2 {
    
    public static int fibo(int n) {
        if(n<=1) {
            return n;
        }
        else return fibo(n-1)+fibo(n-2);
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter n upto which you want sum in the fibonacci series : ");
        int n=sc.nextInt();
        int sum=0;
        for(int i=0;i<n;i++) {
            sum=sum+fibo(i);
        }
        System.out.println("sum upto " + n + " terms of fibonacci series is " + sum);
    }
}
