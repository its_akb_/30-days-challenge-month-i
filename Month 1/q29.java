import java.util.*;

public class q29 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter n : ");
		int n=sc.nextInt();
		String s="0.5";
		for(int i=0;i<n;i++) {
			if(i!=n-1) {
				System.out.print(s + "+");
			} else {
				System.out.print(s);
			}
			s=s+"5";
		}
		System.out.println();
		double c=Math.pow(10, n);
		double res1=1/c;
		double res2=1-res1;
		double res3=0.1111111111111111*res2;
		double res4=n-res3;
		double result=0.5555555555555556*res4;
		System.out.println("Sum = " + result);
	}
}
