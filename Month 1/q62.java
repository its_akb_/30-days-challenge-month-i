import java.util.*;

public class q62 {
    public static boolean checkpal(int a,int b) {
		return a==b;
	}
	
	public static int reverse(int n) { 
		int res=0;
		while(n>0) {
			int c=n%10;
			res=res*10+c;
			n/=10;
		}
		return res;
	}
	
	public static boolean checkprime(int n) {
		boolean c=true;
		for(int i=2;i<n;i++) {
			if(n%i==0) {
				c=false;
				break;
			}
		}
		return c;
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter n : ");
		int n=sc.nextInt();
		System.out.print("Example of prime palindromes are ");
		for(int i=2;i<=n;i++) {
			int temp=reverse(i);
			if(checkprime(i) && checkpal(i, temp)) {
				System.out.print(i + ",");
			}
		}
		System.out.println(" and so on.");
	}
}
