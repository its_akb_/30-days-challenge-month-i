import java.util.*;

public class q81 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a even array size : ");
		int n=sc.nextInt();
		int a[]=new int[n];
		System.out.println("Enter Array element : ");
		for(int i=0;i<a.length;i++) {
			a[i]=sc.nextInt();
		}
		int res1=0,res2=0;
		for(int i=0;i<n/2;i++) {
			res1=res1+a[i];
		}
		for(int i=n/2;i<n;i++) {
			res2=res2+a[i];
		}
		int dif=Math.abs(res1-res2);
		System.out.println("To make the array balance we need to add " + dif);
	}
}