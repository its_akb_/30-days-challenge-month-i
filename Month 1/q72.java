import java.util.*;

public class q72 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size : ");
		int n=sc.nextInt();
		int arr[]=new int[n];
		int index[]=new int[n];
		int res[]=new int[n];
		System.out.print("Enter elements of arrays : ");
		for(int i=0;i<n;i++) {
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter index : ");
		for(int i=0;i<n;i++) {
			index[i]=sc.nextInt();
		}
		for(int i=0;i<n;i++) {
			res[index[i]]=arr[i];
		}
		Arrays.sort(index);
		for(int i=0;i<n;i++) {
			if(i==0) System.out.print("[" + res[i] + ", ");
			else if(i==n-1) System.out.print(res[i] + "]");
			else System.out.print(res[i] + ", ");
		}
		System.out.println();
		for(int i=0;i<n;i++) {
			if(i==0) System.out.print("[" + index[i] + ",  ");
			else if(i==n-1) System.out.print(index[i] + "]");
			else System.out.print(index[i] + ",  ");
		}
	}
}
