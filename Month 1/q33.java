import java.util.*;

public class q33 {
    public static void sortmark(int[] arr) {
		Arrays.sort(arr);
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter total testcase : ");
		int t=sc.nextInt();
		for(int i=0;i<t;i++) {
			System.out.println("Enter no.of question paper : ");
			int n=sc.nextInt();
			int arr[]=new int[n];
			System.out.println("Enter marks : ");
			for(int j=0;j<n;j++) {
				arr[j]=sc.nextInt();
			}
			sortmark(arr);
		}
	}
}
