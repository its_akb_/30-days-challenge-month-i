import java.math.*;

public class q9 {
    public static void main(String[] args) {
        BigInteger j=new BigInteger("1");
        BigInteger res=new BigInteger("0");
        for(int i=1;i<=100;i++) {
            j=j.multiply(BigInteger.valueOf(i));
        }
        while(j!=BigInteger.valueOf(0)) {
            res=res.add(j.mod(BigInteger.valueOf(10)));
            j=j.divide(BigInteger.valueOf(10));
        }
        System.out.println("sum of the digits in the number 100! : " + res);
    }    
}
