import java.util.*;
import java.math.*;

public class q93 {
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		BigInteger res=new BigInteger("2");
		BigInteger result=new BigInteger("0");
		res=res.pow(1000);
		while(res!=BigInteger.valueOf(0)) {
			BigInteger c=res.mod(BigInteger.valueOf(10));
			result=result.add(c);
			res=res.divide(BigInteger.valueOf(10));
		}
		System.out.println("sum of digits of 2^1000 : " + result);
	}
}