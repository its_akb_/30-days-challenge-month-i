import java.util.Scanner;

public class q7 {
    
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter side of square : ");
        double s=sc.nextDouble();
        double rad=s/2;
        double area=3.14*Math.pow(rad, 2);
        System.out.print("Area of the circle inside the square is ");
        System.out.printf("%.2f",area);
    }
}
