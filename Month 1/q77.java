import java.util.Scanner;

public class q77 {
    
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter array size : ");
        int n=sc.nextInt();
        int count=0;
        int[] x=new int[n];
        for(int i=0;i<n;i++) {
            System.out.println("Enter element at position " + i);
            x[i]=sc.nextInt();
        }
        System.out.println("Enter num : ");
        int num=sc.nextInt();
        for(int i=0;i<n;i++) {
            for(int j=i+1;j<n;j++) {
                int temp=Math.abs(x[i]-x[j]);
                if(temp==num) {
                    count++;
                }
            }
        }
        System.out.println(count);
    }

}
