import java.util.Scanner;

public class q90 {
    
    static int rev=0;
	public static int reverse(int n) {
		if(n>0) {
			int rem=n%10;
			rev=rev*10+rem;
			reverse(n/10);
		}
		else 
			return rev;
		return rev;
	}
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter the integer : ");
        int n=sc.nextInt();
        System.out.println("Reversed integer : " + reverse(n));
    }
}
