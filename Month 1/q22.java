import java.util.*;

public class q22 {
    public static int multiply(int m,int n) {
        int result=0;
        while(n>0) {
            if(n%2!=0) {
                result+=m;
            }
            m=m<<1;
            n=n>>1;
        }
        return result;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
		System.out.println("Enter two number to multiply");
		int m=sc.nextInt();
		int n=sc.nextInt();
		System.out.println(m + "X" + n + " = " + multiply(m, n));
    }
}
