import java.util.*;
import java.math.*;

public class q95 {
    
    /*This code might take some time as the operation is big*/

	public static boolean checkprime(int n) {
		boolean flag=true;
		for(int i=2;i<n;i++) {
			if(n%i==0) {
				flag=false;
			}
		}
		return flag;
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		BigInteger sum=new BigInteger("0");
		for(int i=2;i<=2000000;i++) {
			if(checkprime(i)) {
				sum=sum.add(BigInteger.valueOf(i));
			}
		}
		System.out.println(sum);
	}
}

