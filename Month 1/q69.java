import java.util.*;

public class q69 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter n : ");
		int n=sc.nextInt();
		int sum=0;
		System.out.print("Even divider sum ");
		for(int i=2;i<=n;i+=2) {
			if(n%i==0) {
				sum=sum+i;
				if(i<n) System.out.print(i + " + ");
				else System.out.print(i + " = ");
			}
		}
		System.out.print(sum);
	}
}
