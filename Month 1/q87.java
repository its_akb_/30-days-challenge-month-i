import java.util.Scanner;

public class q87 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Input a float number: ");
		float a=sc.nextFloat();
		float res=Math.round(a);
		System.out.print("The rounded value of ");
		System.out.printf("%.6f",a);
		System.out.printf(" is: " + "%.2f",res);
	}
}
