import java.util.*;

public class q11 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the total no. of paths you want to compare for : ");
		int t=sc.nextInt();
		double res[]=new double[t];
		for(int i=0;i<t;i++) {
			double x1,y1,x2,y2;
			if((i+1)%10==1 && (i+1)%100!=11) {
				System.out.println("Enter the constraints for " + (i+1) + "st path: ");
				System.out.print("Enter X and Y coordinates of Initial position: ");
				x1=sc.nextDouble();
				y1=sc.nextDouble();
				System.out.print("Enter X and Y coordinates of Final position: ");
				x2=sc.nextDouble();
				y2=sc.nextDouble();
			} else if((i+1)%10==2 && (i+1)%100!=12) {
				System.out.println("Enter the constraints for " + (i+1) + "nd path: ");
				System.out.print("Enter X and Y coordinates of Initial position: ");
				x1=sc.nextDouble();
				y1=sc.nextDouble();
				System.out.print("Enter X and Y coordinates of Final position: ");
				x2=sc.nextDouble();
				y2=sc.nextDouble();
			} else if((i+1)%10==3 && (i+1)%100!=13) {
				System.out.println("Enter the constraints for " + (i+1) + "rd path: ");
				System.out.print("Enter X and Y coordinates of Initial position: ");
				x1=sc.nextDouble();
				y1=sc.nextDouble();
				System.out.print("Enter X and Y coordinates of Final position: ");
				x2=sc.nextDouble();
				y2=sc.nextDouble();
			} else {
				System.out.println("Enter the constraints for " + (i+1) + "th path: ");
				System.out.print("Enter X and Y coordinates of Initial position: ");
				x1=sc.nextDouble();
				y1=sc.nextDouble();
				System.out.print("Enter X and Y coordinates of Final position: ");
				x2=sc.nextDouble();
				y2=sc.nextDouble();
			}
			double a=Math.pow(x2-x1, 2);
			double b=Math.pow(y2-y1, 2);
			res[i]=Math.pow(a+b, 0.5);	
		}
		for(int i=0;i<res.length;i++) {
			System.out.printf("%.2f",res[i]);
			System.out.print(" Units Distance will be covered in path " + (i+1) + ".");
			System.out.println();
		}
		double temp=res[0];
		for(int i=0;i<res.length;i++) {
			if(res[i]<temp) {
				temp=res[i];
			}
		}
		int index=0;
		for(int i=0;i<res.length;i++) {
			if(res[i]==temp) {
				index=i;
			}
		}
		System.out.println("You should go with path " + (index+1) + ".");
	}
}
